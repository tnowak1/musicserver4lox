<?php
//Deutsch
//General
$language['GENERAL_SAVE'] = 'Speichern';
$language['GENERAL_CLOSE'] = 'Schließen';
$language['GENERAL_YES'] = 'Ja';
$language['GENERAL_NO'] = 'Nein';
$language['GENERAL_MANUAL'] = 'Handbuch';
$language['GENERAL_FORUM'] = 'Loxforum';
$language['GENERAL_START'] = 'Start';
$language['GENERAL_STOP'] = 'Stop';
$language['GENERAL_RESTART'] = 'Neustart';
$language['GENERAL_TEST'] = 'Test';
$language['GENERAL_SETTINGS'] = 'Einstellungen';
$language['GENERAL_EQ'] = 'Equalizer';
$language['GENERAL_TT_START'] = 'Startet den Service';
$language['GENERAL_TT_STOP'] = 'Stoppt den Service';
$language['GENERAL_TT_RESTART'] = 'Stoppt den Service und startet ihn neu';
$language['GENERAL_TT_TEST'] = 'Testet das Gerät';
$language['GENERAL_TT_CONTROLELEMENTS'] = 'Klicke für Steuerelemente';
$language['GENERAL_TT_SERVICE_RUNNING'] = 'Service läuft';
$language['GENERAL_TT_SERVICE_STOPED'] = 'Service gestoppt';
$language['GENERAL_TT_SERVICE_PLAYER_STANDBY'] = 'Service läuft - Player im Standby';
$language['GENERAL_TT_SERVICE_REACHABLE'] = 'Service erreichbar';
$language['GENERAL_TT_SERVICE_UNREACHABLE'] = 'Service nicht erreichbar';
$language['GENERAL_TT_REACHABLE'] = 'erreichbar';
$language['GENERAL_TT_UNREACHABLE'] = 'nicht erreichbar';
$language['GENERAL_TT_SETTINGS'] = 'Einstellungen';

//Languages
$language['GENERAL_LANG_DE'] = 'deutsch';
$language['GENERAL_LANG_EN'] = 'englisch';

// Power
$language['POWER_PAGE_TITLE'] = 'MS4L Power';
$language['POWER_SHUTDOWN'] = 'System ausschalten';
$language['POWER_REBOOT'] = 'System neu starten';
$language['POWER_MODAL_SHUTDOWN_TITLE'] = 'System herunter fahren';
$language['POWER_MODAL_SHUTDOWN_MSG'] = 'Bist du sicher das du das System herunter fahren willst?';
$language['POWER_MODAL_REBOOT_TITLE'] = 'System neu starten';
$language['POWER_MODAL_REBOOT_MSG'] = 'Bist du sicher das du das System neu starten willst?';
$language['POWER_SHUTDOWN_MSG'] = 'System fährt herunter...';
$language['POWER_REBOOT_MSG'] = 'System führt einen Neustart durch...';
$language['POWER_SHUTDOWN_BACK'] = 'Zurück zum Login';

//Navigaion Main
$language['NAV_MAIN_DASHBOARD'] = 'Dashboard';
$language['NAV_MAIN_SETTINGS'] = 'Einstellungen';
$language['NAV_MAIN_SETTINGS_PLAYER'] = 'Zonen';
$language['NAV_MAIN_SETTINGS_PLAYER_INTERNAL'] = 'intern';
$language['NAV_MAIN_SETTINGS_PLAYER_EXTERNAL'] = 'extern';
$language['NAV_MAIN_SETTINGS_EQUALIZER'] = 'Equalizer';
$language['NAV_MAIN_SETTINGS_TTS'] = 'TTS/Alarm/Klingel';
$language['NAV_MAIN_SETTINGS_SERVICES'] = 'Services';
$language['NAV_MAIN_SETTINGS_SUB_TTS'] = 'TTS/CTS';
$language['NAV_MAIN_SETTINGS_SUB_T2T'] = 'Travel2TTS (t2t)';
$language['NAV_MAIN_SETTINGS_SUB_W2T'] = 'Weather2TTS (w2t)';
$language['NAV_MAIN_SETTINGS_SUB_RING'] = 'Klingel/Wecker/Alarm';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_PM'] = 'PowerManager';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_SQ2LOX'] = 'Squeeze2Lox / MusicServerGateway';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_FM'] = 'FollowMe';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_FC'] = 'FritzCallMonitor';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_T5'] = 'T5 Receiver';
$language['NAV_MAIN_SETUP'] = 'Setup';
$language['NAV_MAIN_SETUP_SOUNDCARD'] = 'Soundkarten';
$language['NAV_MAIN_SETUP_NETWORK'] = 'Netzwerk';
$language['NAV_MAIN_SETUP_NETWORK_SETTINGS'] = 'Netzwerk Einstellungen';
$language['NAV_MAIN_SETUP_NETWORK_MOUNT'] = 'Netzlaufwerk einbinden';
$language['NAV_MAIN_SETUP_EMAIL'] = 'eMail';
$language['NAV_MAIN_SETUP_STATISTICS'] = 'Statistik';
$language['NAV_MAIN_SETUP_LMS'] = 'Logitech Media Server';
$language['NAV_MAIN_SETUP_MINISERVER'] = 'MiniServer';
$language["NAV_MAIN_SETUP_PLAYER"] = 'Zonen';
$language["NAV_MAIN_SETUP_PLAYER_INTERNAL"] = 'intern';
$language["NAV_MAIN_SETUP_PLAYER_EXTERNAL"] = 'extern';
$language['NAV_MAIN_TOOLS'] = 'Tools';
$language['NAV_MAIN_TOOLS_UPDATE'] = 'Update';
$language['NAV_MAIN_TOOLS_BACKUP'] = 'Backup';
$language['NAV_MAIN_TOOLS_SERVERRESET'] = 'MS4L zurücksetzen';
$language['NAV_MAIN_TOOLS_LOXHELPER'] = 'Lox-Config Helper';
$language['NAV_MAIN_TOOLS_LOXHELPER_EVENTCREATOR'] = 'Event Creator';
$language['NAV_MAIN_TOOLS_LOXHELPER_TEMPLATECREATOR'] = 'Template Creator';
$language['NAV_MAIN_HELP'] = 'Hilfe';
$language['NAV_MAIN_HELP_MANUAL'] = 'Handbuch';
$language['NAV_MAIN_HELP_FORUM'] = 'LoxForum';
$language['NAV_MAIN_HELP_SUPPORT_DATA'] = 'Support-Daten';
$language["NAV_MAIN_HELP_SUPPORT_LOGVIEWER"] = 'Log-Datei Viewer';
$language['NAV_MAIN_HELP_ABOUT'] = 'Über';

//Naviagtion Header
$language['NAV_HEADER_TT_SET_LANGUAGE'] = 'Sprache auswählen';
$language['NAV_HEADER_TT_SET_USER'] = 'Nutzer-Einstellungen';
$language['NAV_HEADER_TT_UPDATE'] = 'Update';
$language['NAV_HEADER_TT_UPDATE_AVAILABLE'] = 'Update verfügbar';
$language['NAV_HEADER_TT_POWER'] = 'Power-Menü';
$language['NAV_HEADER_TT_LOGOUT'] = 'Nutzer ausloggen';

//Login-Page
$language['LOGIN_PAGE_TITLE'] = 'MusicServer4Lox Login';
$language['LOGIN_FORM_TITLE'] = 'MusikServer Login';
$language['LOGIN_FORM_USER'] = 'Benutzername';
$language['LOGIN_FORM_PASS'] = 'Passwort';
$language['LOGIN_FORM_FORGOT_PASS'] = 'Passwort vergessen?';
$language['LOGIN_FORM_FORGOT_RESET'] = 'zurücksetzen';
$language['LOGIN_FORM_FORGOT_LOGIN'] = 'Login';
$language['LOGIN_FORM_MODAL_TITLE'] = 'Username / Passwort ist falsch!';
$language['LOGIN_FORM_MODAL_MSG'] = 'Dein Benutzername oder Dein Passwort scheinen falsch zu sein!<br>Bitte erneut versuchen...';

//Password Reset
$language['PASSWDRESE_PAGE_TITLE'] = 'MusicServer4Lox Passwort-Reset';
$language["PASSWDRESET_TITLE"] = 'Passwort zurücksetzen';
$language["PASSWDRESET_TEXT"] = 'Wenn du den Mail-Server konfiguriert hast, wird dir eine Email mit einem neuen Passwort zugesendet.'; 
$language["PASSWDRESET_RESET"] = 'Passwort jetzt zurücksetzen';
$language["PASSWDRESET_MODAL_TITLE_MAILOK"] = 'eMail Versand OK.';
$language["PASSWDRESET_MODAL_TITLE_MAILNOK"] = 'eMail Versand fehlgeschlagen';
$language["PASSWDRESET_MODAL_MSG_MAILOK"] = 'Die eMail wurde erfolgreich an dich versendet.';
$language["PASSWDRESET_MODAL_MSG_MAILNOK"] = 'Die eMail konnte nicht versendet werden.<br>Mail-Server konfiguriert?';
$language["PASSWDRESET_MAIL_SUBJECT"] = 'MS4L - neues Passwort';
$language["PASSWDRESET_MAIL_TEXT"] = 'Hallo {realname},<br><br>Du hast ein neues Passwort für deinen MusicServer4Lox angefordert.<br><br>	Dein Username: {username} <br>Dein neues Passwort : {password} <br><br>Du kannst dich jetzt mit dem neuen Passwort einloggen.<br><br>Dein MusicServer4Lox-Team';

//Dashboard (index.php)
$language['DASHBOARD_PAGE_TITLE'] = 'MS4L Dashboard';
$language['DASHBOARD_TITLE'] = 'Dashboard';
$language['DASHBOARD_TITLE_MUSICSERVER'] = 'MuiskServer';
$language['DASHBOARD_TITLE_NETWORK'] = 'Netzwerk';
$language['DASHBOARD_TITLE_SERVICES'] = 'Services';
$language['DASHBOARD_TITLE_ZONES'] = 'Musik-Zonen';
$language['DASHBOARD_UPTIME'] = 'Uptime';
$language['DASHBOARD_DAY'] = 'Tag(e)';
$language['DASHBOARD_TOTAL'] = 'total';
$language['DASHBOARD_FREE'] = 'frei';
$language['DASHBOARD_USED'] = 'benutzt';
$language['DASHBOARD_HARDWARE'] = 'Hardware';
$language['DASHBOARD_CPU'] = 'Prozessor';
$language['DASHBOARD_RAM'] = 'Arbeitsspeicher';
$language['DASHBOARD_HDD'] = 'Festplatte';
$language['DASHBOARD_OS'] = 'Betriebssystem';
$language['DASHBOARD_HOSTNAME'] = 'Netzwerkname';
$language['DASHBOARD_IP'] = 'IP Adresse';
$language['DASHBOARD_NETMASK'] = 'Netmask';
$language['DASHBOARD_GATEWAY'] = 'Gateway';
$language['DASHBOARD_DNS'] = 'DNS Server';
$language['DASHBOARD_INET'] = 'Internet';
$language['DASHBOARD_MINISERVER'] = 'MiniServer';
$language['DASHBOARD_PACKAGES_LOSS']= 'Paketverlust';
$language['DASHBOARD_MS'] = 'MusicServer';
$language['DASHBOARD_START_ALL'] = 'Start aller Services';
$language['DASHBOARD_STOP_ALL'] = 'Stop aller Services';
$language['DASHBOARD_RESTART_ALL'] = 'Neustart aller Services';
$language['DASHBOARD_LMS'] = 'Logitech Media Server';
$language['DASHBOARD_LMS_WEB'] = 'LMS WebUI';
$language['DASHBOARD_LMS_TELNET'] = 'LMS Telenet';
$language['DASHBOARD_PM'] = 'Powermanager';
$language['DASHBOARD_SQ2LOX'] = 'Squeeze2lox / MS-Gateway';
$language['DASHBOARD_FC'] = 'FritzCall-Monitor';
$language['DASHBOARD_T5'] = 'T5 Receiver';
$language['DASHBOARD_SERVER_NOT_REACHABLE'] = 'Server nicht erreichbar!';
$language['DASHBOARD_NO_INTERNAL_PLAYER'] = 'Keine internen Zonen angelegt.';
$language['DASHBOARD_NO_EXTERNAL_PLAYER'] = 'Keine externen Zonen angelegt.';
$language['DASHBOARD_VISIT_WEBUI'] = 'LMS-WebUI';
$language['DASHBOARD_VISIT_WEBUI_SETTINGS'] = 'LMS-WebUI-Einstellungen';
$language['DASHBOARD_TT_PLAY'] = 'Player spielt Musik';
$language['DASHBOARD_TT_PAUSE'] = 'Player steht auf Pause';
$language['DASHBOARD_TT_STOP'] = 'Player steht auf Stop';
$language['DASHBOARD_TT_VISIT_WEBUI'] = 'WebUI aufrufen';
$language['DASHBOARD_TT_VISIT_WEBUI_SETTINGS'] = 'WebUI-Settings aufrufen';

//About
$language['ABOUT_PAGE_TITLE'] = 'MS4L - Über';
$language["ABOUT_TITLE"] = 'Über MusicServer4Lox';
$language["ABOUT_TITLE_COPYRIGHT"] = 'Copyright';
$language["ABOUT_COPYRIGHT_MSG"] = '
MusicServer4Lox wurde von mir (Dieter Schmidberger) entwickelt und im loxforum.com vorgestellt.
<br>Diese Software wird kostenlos angeboten, ist aber urheberrechtlich geschützt.
<br>Das bedeutet, dass die Software kostenlos weiterverbreitet werden darf, aber die Veränderung des Codes ist nur mit ausdrücklicher Genehmigung des Copyright-Inhabers erlaubt.
<br>Das betrifft nur die Softwareteile die von mir selber geschrieben wurden.';
$language["ABOUT_TITLE_LICENCE"] = 'Lizenzen';
$language["ABOUT_TITLE_DISCLAIMER"] = 'Haftungsausschluss';
$language["ABOUT_DISCLAIMER_MSG"] = '
Die Software wurde nach bestem Wissen und Gewissen von mir und für mich persönlich erstellt.
<br>In der Hoffnung, dass sie auch für andere nützlich ist, habe ich sie veröffentlicht. 
<br>Es gibt ausdrücklich keine Garantie für Funktionen und Funktionen. Die Benutzung der Software erfolgt auf eigene Gefahr!';
$language["SUPPORT_TITLE_CREDITS"] = 'Danksagungen';
$language["SUPPORT_PICTURES"] = 'Bilder';
$language["SUPPORT_TITLE_CODING"] = 'Programmierung';
$language["SUPPORT_TITLE_TRANSLATION"] = 'Übersetzung';

//Settings Player internen
$language['SET_PLAYER_PAGE_TITLE'] = 'MS4L Zonen Einstellungen';
$language['SET_PLAYER_TITLE'] = 'Zonen Einstellungen (intern)';
$language['SET_PLAYER_TITLE_EXT'] = 'Zonen Einstellungen (extern)';
$language['SET_PLAYER_TITLE_ZONES'] = 'Zonen';
$language['SET_PLAYER_TITLE_ZONE'] = 'Zone';
$language['SET_PLAYER_TITLE_SOUNDCARD'] = 'Soundkarte';
$language['SET_PLAYER_TITLE_SQ2LOX'] = 'Sqeeze2Lox';
$language['SET_PLAYER_TITLE_PM'] = 'PowerManager';
$language['SET_PLAYER_DEL'] = 'Zone löschen';
$language['SET_PLAYER_PLAYERNEW'] = 'Neue Zone';
$language['SET_PLAYER_MANUAL'] = 'Zonen-Name / Zonen-Mac manuell eingeben';
$language['SET_PLAYER_ZONENUMBER'] = 'Zonen Nummer';
$language['SET_PLAYER_ZONENAME'] = 'Zonen Name';
$language['SET_PLAYER_ZONEMAC'] = 'Mac-Adresse';
$language['SET_PLAYER_ZONEALSA'] = 'Alsa Einstellungen';
$language['SET_PLAYER_ZONESOUNDCARD'] = 'Zonen-Soundkarte';
$language['SET_PLAYER_CH1L'] = 'Kanal 1 links';
$language['SET_PLAYER_CH1R'] = 'Kanal 1 rechts';
$language['SET_PLAYER_CH2L'] = 'Kanal 2 links';
$language['SET_PLAYER_CH2R'] = 'Kanal 2 rechts';
$language['SET_PLAYER_CH3L'] = 'Kanal 3 links';
$language['SET_PLAYER_CH3R'] = 'Kanal 3 rechts';
$language['SET_PLAYER_CH4L'] = 'Kanal 4 links';
$language['SET_PLAYER_CH4R'] = 'Kanal 4 rechts';
$language['SET_PLAYER_MSG_USE'] = 'MusicServer App Gateway aktivieren';
$language["SET_PLAYER_MSG_ZONENR"] = 'Audio Zonen Nummer';
$language['SET_PLAYER_SQ2LOX_USE'] = 'Squeeze2Lox für diese Zone aktivieren';
$language['SET_PLAYER_SQ_VTITITLE'] = 'VTI Artist/Title';
$language['SET_PLAYER_SQ_VTIMODE'] = 'VTI Mode';
$language['SET_PLAYER_SQ_VIVOLUME'] = 'VI Volume';
$language['SET_PLAYER_SQ_VIPOWER'] = 'VI Power';
$language['SET_PLAYER_SQ_VTISYNC'] = 'VTI Sync';
$language['SET_PLAYER_PM_USE'] = 'Powermanager für diese Zone aktivieren';
$language['SET_PLAYER_PM_VIAMP'] = 'VI Amplifer';
$language['SET_PLAYER_PM_URLON'] = 'URL - Ein';
$language['SET_PLAYER_PM_URLOFF'] = 'URL - Aus';
$language['SET_PLAYER_MODAL_WRITE_TITLE'] = 'Einstellungen speichern';
$language['SET_PLAYER_MODAL_WRITEOK_MSG'] = 'Die Einstellungen wurden erfolgreich gespeichert.<br>Der Player und alle Services wurden neu gestartet.';
$language['SET_PLAYER_MODAL_WRITENOK_MSG'] = 'Die Einstellungen konnten nicht gesichert werden!';
$language['SET_PLAYER_MODAL_DEL_TITLE'] = 'Zone löschen';
$language['SET_PLAYER_MODAL_DELOK_MSG'] = 'Die Zone wurde erfolgreich gelöscht.';
$language['SET_PLAYER_MODAL_DELNOK_MSG'] = 'Die Zone konnte nicht gelöscht werden.';
$language['SET_PLAYER_MODAL_DELASK_MSG'] = 'Bist du sicher das du die Zone löschen willst?';
$language['SET_PLAYER_REQ_NAME'] = 'Name muss vergeben werden. (A-Z, a-z, 0-9, _, -)';
$language['SET_PLAYER_REQ_ALSA'] = 'Parameter werden benötigt. Std. ist(80:4::)';
$language['SET_PLAYER_REQ_SC'] = 'Dieser Kanal wir benötigt.';
$language['SET_PLAYER_REQ_URL'] = 'Bitte korrektes URL-Format eingeben. (http://xxxx.xx)';
$language['SET_PLAYER_TT_NAME'] = 'A-Z a-z 0-9 _ - Leerzeichen';
$language['SET_PLAYER_TT_ALSA'] = 'Nur ändern bei Problemen.';
$language['SET_PLAYER_TT_FROMSYSTEM'] = 'Wird vom System vergeben.';
$language['SET_PLAYER_TT_CH'] = 'Kanal auswählen.';
$language['SET_PLAYER_TT_CHOPT'] = 'optionalen Kanal auswählen.';
$language['SET_PLAYER_TT_NUMBERSONLY'] = 'Nur Zahlen eingeben.';
$language['SET_PLAYER_TT_URL'] = 'Format http://xxxx.xx eingeben.';
$language['SET_PLAYER_TT_MSG_NUMBER'] = 'Nummer der Audio-Zone wählen die in der Lox-Config für diese Zone genutzt wird.';

//User
$language['SET_USER_PAGE_TITLE'] = 'MS4L Benutzer Einstellungen';
$language['SET_USER_TITLE'] = 'Benutzer Einstellungen';
$language['SET_USER_TITLE_USER'] = 'Benutzer';
$language['SET_USER_NAME'] = 'Nutzername';
$language['SET_USER_PASSWORD'] = 'Passwort';
$language['SET_USER_PASSWORD_CONFIRM'] = 'Passwort Wiederholung';
$language['SET_USER_REQ_NAME'] = 'Name muss vergeben werden. (A-Z, a-z, 0-9, _, -)';
$language['SET_USER_REQ_PASS'] = 'Passwort muss vergeben werden.  (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language['SET_USER_REQ_PASS_CONFIRM'] = 'Passwort muss mit erster Eingabe übereinstimmen.';
$language['SET_USER_MODAL_WRITE_TITLE'] = 'Einstellungen speichern';
$language['SET_USER_MODAL_WRITEOK_MSG'] = 'Die Einstellungen wurden erfolgreich gespeichert.';
$language['SET_USER_MODAL_WRITENOK_MSG'] = 'Die Einstellungen konnten nicht gesichert werden!';

//Equalizer
$language['SET_EQ_PAGE_TITLE'] = 'MS4L-Equalizer';
$language["SET_EQ_TITLE"] = 'Equalizer';
$language["SET_EQ_TITLE_ZONES"] = 'Zonen';
$language["SET_EQ_LOAD_PRESET"] = 'Presets';
$language["SET_EQ_LOAD"] = 'Laden User-Sets';
$language["SET_EQ_SAVE"] = 'Speichern User-Sets';
$language["SET_EQ_LOAD_PRESET_FLAT"] = 'Flat';
$language["SET_EQ_LOAD_PRESET_ROCK"] = 'Rock';
$language["SET_EQ_LOAD_PRESET_LOUD"] = 'Loudness';
$language["SET_EQ_LOAD_PRESET_POP"] = 'Pop';
$language["SET_EQ_LOAD_PRESET_CLASSIC"] = 'Klassik';
$language["SET_EQ_LOAD_USER1"] = '1';
$language["SET_EQ_SAVE_USER1"] = '1';
$language["SET_EQ_LOAD_USER2"] = '2';
$language["SET_EQ_SAVE_USER2"] = '2';
$language["SET_EQ_LOAD_GLOBAL"] = 'Global';
$language["SET_EQ_SAVE_GLOBAL"] = 'Global';
$language['SET_EQ_MODAL_WRITE_TITLE'] = 'Einstellungen speichern';
$language['SET_EQ_MODAL_WRITEOK_MSG'] = 'Die Einstellungen wurden erfolgreich gespeichert.';
$language['SET_EQ_MODAL_WRITENOK_MSG'] = 'Die Einstellungen konnten nicht gesichert werden!';
$language["SET_EQ_MSG"] = 'MuiscServerGateway ist für diese Zone aktiv. Equalizer über App einstellen.';

//Setup Soundcard
$language['SET_SC_PAGE_TITLE'] = 'MS4L-Soundkarten Setup';
$language["SET_SC_TITLE"] = 'Soundkarten Setup';
$language["SET_SC_TITLE_SC"] = 'Soundkarten';
$language["SET_SC_TITLE_SC_SYSTEM"] = 'Soundkarten erkannt';
$language["SET_SC_SC1_USE"] = 'Soundkarte 1 aktivieren';
$language["SET_SC_SC2_USE"] = 'Soundkarte 2 aktivieren';
$language["SET_SC_SC3_USE"] = 'Soundkarte 3 aktivieren';
$language["SET_SC_SC4_USE"] = 'Soundkarte 4 aktivieren';
$language["SET_SC_SC5_USE"] = 'Soundkarte 5 aktivieren';
$language["SET_SC_SC6_USE"] = 'Soundkarte 6 aktivieren';
$language["SET_SC_SC7_USE"] = 'Soundkarte 7 aktivieren';
$language["SET_SC_DEVICENR"] = 'Device Nummer';
$language["SET_SC_CHANNEL"] = 'Anzahl Kanäle';
$language["SET_SC_SAMPLING"] = 'Samplingrate';
$language['SET_SC_REQ_DEVCIENR'] = 'Die Device-Nummer wird benötigt';
$language['SET_SC_REQ_CHANNEL'] = 'Die Kanal-Anzahl wird benötigt';
$language['SET_SC_REQ_SAMPLING'] = 'Die Samplingrate wird benötigt';
$language['SET_SC_MODAL_WRITE_TITLE'] = 'Einstellungen speichern';
$language['SET_SC_MODAL_WRITEOK_MSG'] = 'Die Einstellungen wurden erfolgreich gespeichert.';
$language['SET_SC_MODAL_WRITENOK_MSG'] = 'Die Einstellungen konnten nicht gesichert werden!';

//Update
$language['UPDATE_PAGE_TITLE'] = 'MS4L-Update';
$language["UPDATE_TITLE"] = 'Update';
$language["UPDATE_TITLE_MUSICSERVER"] = 'MusicServer';
$language["UPDATE_TITLE_LMS"] = 'Logitech Media Server';
$language["UPDATE_TITLE_SQUEEZE"] = 'Squeezelite';
$language["UPDATE_TITLE_SYSTEM"] = 'System';
$language["UPDATE_VERSION_ONLINE"] = 'Version online';
$language["UPDATE_VERSION_INSTALLED"] = 'Version installiert';
$language["UPDATE_VERSION_PACKAGES"] = 'Paket(e)';
$language["UPDATE_BTN_UPDATE"] = 'Update';
$language["UPDATE_BTN_LOG"] = 'Log-Datei (letztes Update)';
$language["UPDATE_BTN_RELOAD"] = 'Nachdem SUCCESSFULLY UPDATED erscheint,  kannst du hier die Seite aktualisieren';
$language["UPDATE_BTN_CHANGELOG"] = 'Changelog';
$language["UPDATE_MAILER_SUBJECT"] = 'MS4L - Update Information';
$language["UPDATE_MAILER_TEXT"] = 'Hallo {realname},<br><br>Es gibt Neuigkeiten für deinen MusikServer.<br><br>{verions}<br><br>Dein MusicServer4Lox Team.';

//Backup
$language['BACKUP_PAGE_TITLE'] = 'MS4L-Backup';
$language["BACKUP_TITLE"] = 'Backup';
$language["BACKUP_TITLE_MUSICSERVER"] = 'MusicServer';
$language["BACKUP_TITLE_MUSICSERVER_SETTINGS"] = 'MusicServer Einstellungen';
$language["BACKUP_TITLE_LMS"] = 'Logitech Media Server';
$language["BACKUP_TITLE_LMS_FAV"] = 'Logitech Media Server Favoriten';
$language["BACKUP_TITLE_LMS_PL"] = 'Logitech Media Server Playlists';
$language["BACKUP_TITLE_MS_FS"] = 'MusicServer Dateien und Einstellungen';
$language["BACKUP_BTN_CHOOSEFILE"] = 'Datei auswählen';
$language["BACKUP_BTN_BACKUP"] = 'Backup';
$language["BACKUP_BTN_RESTORE"] = 'Restore';
$language["BACKUP_BTN_BACKUPLOG"] = 'Log-Datei (letztes Backup)';
$language["BACKUP_BTN_RESTORELOG"] = 'Log-Datei (letzter Restore)';

//Reset Server
$language['RESET_SERVER_PAGE_TITLE'] = 'MS4L Server-Reset';
$language['RESET_SERVER_TITLE'] = 'Server-Reset';
$language["RESET_SERVER_TITLE_BOX"] = 'Server Einstellungen zurücksetzten';
$language["RESET_SERVER_BTN"] = 'Server zurücksetzen';
$language["RESET_SERVER_MSG"] = '
<br>Nach Klicken des Button werden bis auf das Netzwerk alle Einstellungen des MS4L zurückgesetzt.
<br>Der Logitech Media Server wird nicht zurückgesetzt!
<br>Dies kann nur durch ein Restore eines Backup wieder Rückgängig gemacht werden.';
$language["RESET_SERVER_MSG2"] = 'Die Einstellungen werden zurückgesetzt.<br>Das System führt danach automatisch einen Neustart durch...';
$language["RESET_SERVER_MODAL_TITLE"] = 'Server-Einstellungen zurücksetzten';
$language["RESET_SERVER_MODAL_SHUTDOWN_MSG"] = 'Bist du sicher das du den Server zurücksetzen willst?';

//SupportData
$language['SUPPORT_PAGE_TITLE'] = 'MS4L Support-Daten';
$language['SUPPORT_TITLE'] = 'Support-Daten';
$language["SUPPORT_TITLE_BOX"] = 'Support-Daten';
$language["SUPPORT_DOWNLOAD_BTN"] = 'Download Textdatei für Support-Anfragen im Loxforum';

//Template Generator
$language['TEMPLATE_PAGE_TITLE'] = 'MS4L Template-Creator';
$language['TEMPLATE_TITLE'] = 'Template-Creator';
$language["TEMPLATE_TITLE_BOX"] = 'Download';
$language["TEMPLATE_DOWNLOAD_CLI"] = 'CLI- Template';
$language["TEMPLATE_DOWNLOAD_T5"] = 'T5 - Template';
$language["TEMPLATE_DOWNLOAD_TS"] = 'TS - Template (alternative Bedienung zu T5 (Labmaster)';

//Services
$language['SET_SERVICE'] = 'Services';
$language['SET_SERVICE_PM'] = 'Powermanager';
$language['SET_SERVICE_SQ2LOX'] = 'Squeeze2lox / MSG';
$language['SET_SERVICE_FC'] = 'FritzCall-Monitor';
$language['SET_SERVICE_T5'] = 'T5 Receiver';
$language['SET_SERVICE_FM'] = 'FollowMe';
$language['SET_SERVICE_MODAL_WRITE_TITLE'] = 'Einstellungen speichern';
$language['SET_SERVICE_MODAL_WRITEOK_MSG'] = 'Die Einstellungen wurden erfolgreich gespeichert.';
$language['SET_SERVICE_MODAL_WRITENOK_MSG'] = 'Die Einstellungen konnten nicht gesichert werden!';

//PowerManager
$language['SET_PM_PAGE_TITLE'] = 'MS4L-Service PowerManger';
$language["SET_PM_TITLE"] = 'PowerManager Einstellungen';
$language["SET_PM_TITLE_ZONE"] = 'PowerManager';
$language["SET_PM_TITLE_AMP"] = 'Amp';
$language["SET_PM_TITLE_VOLUMERESET"] = 'Volume Reset';
$language['SET_PM_AUTOSTART'] = 'Autostart aktivieren';
$language['SET_PM_POWEROFF_STOP'] = 'Zone ausschalten nach Stop';
$language['SET_PM_POWEROFF_PAUSE'] = 'Zone ausschalten nach Pause';
$language['SET_PM_AMPALL'] = 'Amplifer-Steuerung für alle Zonen aktivieren';
$language['SET_PM_AMPOFF'] = 'Amplifer-Zeit ausschalten';
$language['SET_PM_AMPSTARTUP'] = 'Amplifer-Zeit einschalten';
$language['SET_PM_VIAMPALL'] = 'Amplifer VI';
$language['SET_PM_URLALLON'] = 'URL - Ein';
$language['SET_PM_URLALLOFF'] = 'URL - Aus';
$language["SET_PM_VOLUMERESET"] = 'Volume-Reset für interne Zonen aktivieren';
$language["SET_PM_STARTVOLUME"]  = 'Volume-Wert für interne Zonen';
$language["SET_PM_VOLUMERESET_EXT"] = 'Volume-Reset für externe Zonen aktivieren';
$language["SET_PM_STARTVOLUME_EXT"]  = 'Volume-Wert für externe Zonen';
$language["SET_PM_REQ_POWEROFF"] = 'Hier muss eine Zeit von >= 10 Sekunden eingetragen werden.';
$language["SET_PM_REQ_AMPSTARTUP"] = 'Hier muss ein Wert eintragen werden, auch bei 0 Sekunden.';
$language["SET_PM_REQ_AMPVIAMPALL"] = 'Hier muss ein Wert eingetragen werden, 0 wenn nur URL schalten sollen .';
$language["SET_PM_REQ_STARTVOLUME"] = 'Hier muss eine Start-Volume-Wert > 0 eingesargten werden.';
$language["SET_PM_REQ_AMPURL"] = 'Bitte korrektes URL-Format eingeben. (http://xxxx.xx)';
$language['SET_PM_TT_NUMBERSONLY'] = 'Nur Zahlen eingeben.';
$language['SET_PM_TT_URL'] = 'Format http://xxxx.xx eingeben.';

//Squeezw2Lox
$language['SET_SQ2LOX_PAGE_TITLE'] = 'MS4L-Service Squeeze2Lox / MSG';
$language["SET_SQ2LOX_TITLE"] = 'Squeeze2Lox / MSG Einstellungen';
$language["SET_SQ2LOX_TITLE_BOX"] = 'Squeeze2Lox / MusicServerGateway';
$language['SET_SQ2LOX_AUTOSTART'] = 'Autostart aktivieren';

//T5 Receiver
$language['SET_T5_PAGE_TITLE'] = 'MS4L-Service T5-Receiver';
$language["SET_T5_TITLE"] = 'T5-Receiver Einstellungen';
$language["SET_T5_TITLE_BOX"] = 'T5-Receiver';
$language['SET_T5_AUTOSTART'] = 'Autostart aktivieren';
$language['SET_T5_UDP'] = 'UDP Port';
$language['SET_T5_SAYFAV'] = 'Say Favorite';
$language['SET_T5_SAYFAVVOL'] = 'Say Favorite Volume Boost';
$language['SET_T5_VOLUMESTEP'] = 'Volume Step';
$language["SET_T5_REQ_UDP"] = 'Wert muss zwischen 1-65535 liegen';
$language["SET_T5_TT_UDP"] = 'UDP Port über den der MiniServer Daten an den MS4L sendet';
$language["SET_T5_TT_VOLUMESTEP"] = 'Lautstärke-Stufen um die der Wert je Stufe (klick) erhöht/verringert wird.';
$language["SET_T5_TT_SAYFAVVOL"] = 'Um diesen Wert wird die Lautstärke beim ansagen der Favoriten erhöht.';

//FollowMe
$language['SET_FM_PAGE_TITLE'] = 'MS4L-Service FollowMe';
$language["SET_FM_TITLE"] = 'FollowMe Einstellungen';
$language["SET_FM_TITLE_FM"] = 'FollowMe';
$language["SET_FM_STOPFROM"] = 'Stop From-Zone';
$language["SET_FM_PLAYTO"] = 'Play To-Zone';
$language["SET_FM_VOLUMETO"] = 'Volume From-Zone->To-Zone';
$language["SET_FM_TT_STOPFROM"] = 'Setze die Zone von der die Playliste mitgenommen wird auf Stop.';
$language["SET_FM_TT_PLAYTO"] = 'Setze die Zone auf die die Playliste mitgenommen wird auf Play.';
$language["SET_FM_TT_VOLUMETO"] = 'Volume von der Zone mitnehmen und auf der neuen Zonen setzen.';

//FritzCallMonitor
$language['SET_FC_PAGE_TITLE'] = 'MS4L-Service CallMonitor';
$language["SET_FC_TITLE"] = 'CallMonitor Einstellungen';
$language["SET_FC_TITLE_FC"] = 'CallMonitor';
$language["SET_FC_TITLE_TTS"]= 'Text2Speech';
$language["SET_FC_TITLE_NUMBERS"]= 'Nummern / Namen';
$language["SET_FC_CMOK"] = 'CallMonitor der Fritzbox ist erreichbar.';
$language["SET_FC_CMNOK"] = 'CallMonitor der Fritzbox ist nicht erreichbar. Aktiviert?';
$language["SET_FC_CMIPEMPTY"] = 'IP der Fritzbox wurde noch nicht eingegeben!';
$language["SET_FC_AUTOSTART"]= 'Autostart aktivieren';
$language["SET_FC_IP"]= 'Fritzbox IP';
$language["SET_FC_USER"]= 'Fritzbox Nutzername';
$language["SET_FC_PASS"]= 'Fritzbox Passwort';
$language["SET_FC_TTSVI"] = 'VI Nummer - AuslöseImplus';
$language["SET_FC_CALLERVTI"]  = 'VTI Nummer - Anrufer';
$language["SET_FC_CALLEDVTI"] = 'VTI Nummer - Angerufener';
$language["SET_FC_NUMBERTALKSPEED"] = 'Sprechgeschwindigkeit langsam für Nummern';
$language["SET_FC_NUMBER"] = 'Telefonnummer';
$language["SET_FC_NAME"] = 'Anschlussname';
$language["SET_FC_REQ_IP"] = 'IP muss vergeben werden. (xxx.xxx.xxx.xxx)';
$language["SET_FC_REQ_USER"] = 'Benutzername muss vergeben werden. (A-Z, a-z, 0-9, _, -)';
$language["SET_FC_REQ_PASS"] = 'Passwort muss vergeben werden.  (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language["SET_FC_REQ_TTSVI"] = 'VI für Impuls muss vergeben werden. (nur Zahlen)';
$language["SET_FC_REQ_CALLERVTI"] = 'VTI für Anrufer muss vergeben werden. (nur Zahlen)';
$language["SET_FC_REQ_CALLEDVTI"] = 'VTI für Angerufener muss vergeben werden. (nur Zahlen)';
$language["SET_FC_REQ_NUMBER"] = 'Telefonnummer eingeben';
$language["SET_FC_REQ_NAME"] = 'Name eingeben, (A-Z, a-z, 0-9, _, -)';

//Network
$language["SET_NETWORK"] = 'Netzwerk';
$language["SET_NETWORK_MOUNT"]  = 'Netzwerklaufwerk einbinden';
$language['SET_NET_PAGE_TITLE'] = 'MS4L-Setup Netzwerk';
$language["SET_NET_TITLE"] = 'Netzwerk Einstellungen';
$language["SET_NET_TITLE_NET"] = 'Netzwerk';
$language["SET_NET_TITLE_HOSTNAME"] = 'Hostname';
$language["SET_NET_TITLE_IFACE"] = 'Netzwerk-Interface';
$language["SET_NET_IFACENAME"] = 'Interface-Name';
$language["SET_NET_MACADDRESS"] = 'Mac-Adresse';
$language["SET_NET_HOSTNAME"] = 'Hostname';
$language["SET_NET_HOSTDOMAIN"] = 'Hostname-Endung';
$language["SET_NET_DHCP"] = 'DHCP verwenden';
$language["SET_NET_IP"] = 'IP Adresse';
$language["SET_NET_SUBNET"] = 'Subnet';
$language["SET_NET_GATEWAY"] = 'Gateway';
$language["SET_NET_DNSSERVER"] = 'DNS-Server';
$language['SET_NET_MODAL_WRITE_TITLE'] = 'Einstellungen speichern';
$language['SET_NET_MODAL_WRITEOK_MSG'] = 'Die Einstellungen wurden erfolgreich gespeichert.';
$language['SET_NET_MODAL_WRITEOK_MSG1'] = 'Um die Einstellungen zu aktiveren muss das System neu gestartet werden.';
$language['SET_NET_MODAL_WRITEOK_MSG2'] = 'Soll das System jetzt neu gestartet werden?';
$language['SET_NET_MODAL_WRITENOK_MSG'] = 'Die Einstellungen konnten nicht gesichert werden!';

//NetworkShare
$language['SET_NETSHARE_PAGE_TITLE'] = 'MS4L-Setup Netzwerklaufwerk'; 
$language["SET_NETSHARE_TITLE"] = 'Netzwerklaufwerk einbinden';
$language["SET_NET_TITLE_NETSHARE"] = 'NAS Einstellungen';
$language["SET_NET_AUTOMOUNT"] = 'Netzlaufwerk automatisch einbinden';
$language["SET_NET_IPHOSTNAME"] = 'IP / Hostname NAS';
$language["SET_NET_FOLDER"] = 'Verzeichnis auf dem NAS';
$language["SET_NET_TITLE_AUTH"] = 'NAS Authentifizierung';
$language["SET_NET_LOGIN"] = 'Authentifizierung aktivieren';
$language["SET_NET_USER"] = 'Benutzername';
$language["SET_NET_PASS"] = 'Passwort';
$language["SET_NET_CHARTSET"] = 'Nutze utf8 Zeichensatz anstatt iso8859-1';
$language["SET_NET_FOLDERNAS"] = 'NAS-Verzeichnis';
$language["SET_NET_REQ_IPHOSTNAME"] = 'IP oder Hostname müssen angegeben werden (ohne //)';
$language["SET_NET_REQ_FOLDER"]  = 'Verzeichnis muss angegeben werden (ohne /)';
$language["SET_NET_REQ_USER"] = 'Benutzername muss vergeben werden. (A-Z, a-z, 0-9, _, -)';
$language["SET_NET_REQ_PASS"] = 'Passwort muss vergeben werden.  (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language["SET_NET_BTN_MOUNT"] = 'Laufwerk einhängen';
$language["SET_NET_BTN_UNMOUNT"] = 'Laufwerk aushängen';
$language["SET_NET_MOUNTOK"] = 'Netzlaufwerk ist eingehängt';
$language["SET_NET_MOUNTNOK"]  = 'Netzlaufwerk ist nicht eingehängt';

//Mail
$language['SET_MAIL_PAGE_TITLE'] = 'MS4L-Setup eMail'; 
$language["SET_MAIL_TITLE"] = 'eMail Einstellungen';
$language["SET_MAIL_TITLE_NAME"] = 'eMail / Name';
$language["SET_MAIL_TITLE_SMTP"] = 'SMTP-Server';
$language["SET_MAIL_TITLE_UPDATEMAIL"] = 'Update-Mailer';
$language["SET_MAIL_REALNAME"] = 'Realer Name';
$language["SET_MAIL_EMAILTO"] = 'eMail-Adresse - Empfänger';
$language["SET_MAIL_SMTPSERVER"] = 'SMTP-Server';
$language["SET_MAIL_SMTPPORT"] = 'SMTP-Port';
$language["SET_MAIL_SMTPSECURE"] = 'SMTP-Sicherheit';
$language["SET_MAIL_SMTPUSER"] = 'SMTP-Benutzername';
$language["SET_MAIL_SMTPPASS"] = 'SMTP-Passwort';
$language["SET_MAIL_UPDATEMAIL"] = 'Update-Mailer aktivieren';
$language["SET_MAIL_UPDATEINTERVAL"] = 'Update-Mailer Intervall';
$language["SET_MAIL_UPDATEDAY"] = 'Update-Mailer Tag';
$language["SET_MAIL_UPDATEWEEKLY"] = 'wöchentlich';
$language["SET_MAIL_UPDATEDAYLY"] = 'täglich';
$language["SET_MAIL_UPDATEMO"] = 'Montag';
$language["SET_MAIL_UPDATETU"] = 'Dienstag';
$language["SET_MAIL_UPDATEWE"] = 'Mittwoch';
$language["SET_MAIL_UPDATETH"] = 'Donnerstag';
$language["SET_MAIL_UPDATEFR"] = 'Freitag';
$language["SET_MAIL_UPDATESA"] = 'Samstag';
$language["SET_MAIL_UPDATESU"] = 'Sonntag';
$language["SET_MAIL_REQ_REALNAME"] = 'Name muss eingetragen werden';
$language["SET_MAIL_REQ_EMAILTO"] = 'eMail-Adresse muss eingetragen werden';
$language["SET_MAIL_REQ_SMTPSERVER"] = 'SMTP-Server muss eingetragen werden';
$language["SET_MAIL_REQ_SMTPPORT"] = 'SMTP-Port muss eingetragen werden (1-65535)';
$language["SET_MAIL_REQ_SMTPSECURE"] = 'SMTP-Sicherheit muss eingetragen werden';
$language["SET_MAIL_REQ_SMTPUSER"] = 'SMTP-Benutzername muss eingetragen werden';
$language["SET_MAIL_REQ_SMTPPASS"] = 'SMTP-Passwort muss eingetragen werden';
$language["SET_MAIL_BTN_TESTMAIL"] = 'Test-eMail versenden';
$language["SET_MAIL_TESTOK"] = 'eMail wurde erfolgreich versendet';
$language["SET_MAIL_TESTNOK"] = 'eMail konnte nicht versendet werden.'; 

//MiniServer
$language['SET_MS_PAGE_TITLE'] = 'MS4L MiniServer Einstellungen';
$language['SET_MS_TITLE'] = 'MiniServer Einstellungen';
$language['SET_MS_TITLE_MS'] = 'MiniServer';
$language['SET_MS_IP'] = 'MiniServer IP-Adresse';
$language['SET_MS_USER'] = 'MiniServer Nutzername';
$language['SET_MS_PASSWORD'] = 'MiniServer Passwort';
$language['SET_MS_PASSWORD_CONFIRM'] = 'MiniServer Passwort Wiederholung';
$language['SET_MS_REQ_IP'] = 'IP muss vergeben werden. (xxx.xxx.xxx.xxx)';
$language['SET_MS_REQ_USER'] = 'Benutzername muss vergeben werden. (A-Z, a-z, 0-9, _, -)';
$language['SET_MS_REQ_PASS'] = 'Passwort muss vergeben werden.  (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language['SET_MS_REQ_PASS_CONFIRM'] = 'Passwort muss mit erste Eingabe übereinstimmen.';

//LigitechMediaServer
$language['SET_LMS_PAGE_TITLE'] = 'MS4L Logitech Media Server Einstellungen';
$language['SET_LMS_TITLE'] = 'Logitech Media Server Einstellungen';
$language['SET_LMS_TITLE_LMS'] = 'Logitech Media Server';
$language['SET_LMS_IP'] = 'LMS IP-Adresse (wird vom System vergeben)';
$language['SET_LMS_WEBPORT'] = 'LMS WebUI Port';
$language['SET_LMS_CLIPORT'] = 'LMS CLI Port';
$language['SET_LMS_8'] = 'LMS Version 8 nutzen (Beta)';
$language['SET_LMS_REQ_WEBPORT'] = 'Port für das WebUI muss zwischen 1-65535 liegen';
$language['SET_LMS_REQ_CLIPORT'] = 'Port für das CLI muss zwischen 1-65535 liegen';

//Statisics
$language['SET_STAT_PAGE_TITLE'] = 'MS4L Statistik Einstellungen';
$language['SET_STAT_TITLE'] = 'Statistik Einstellungen';
$language['SET_STAT_TITLE_MS'] = 'Statistik';
$language['SET_STAT_SEND'] = 'Statistik Daten senden';
$language['SET_STAT_SHOW'] = 'Statistik zeigen';
$language['SET_STAT_UUID'] = 'UUID (Universally Unique IDentifier)';

//TTS
$language["SET_TTS_TTS"] = 'TTS / CTS';
$language["SET_TTS_RING"] = 'Klingel/Alarm/Wecker';
$language["SET_TTS_WEATHER"] = 'Weather2TTS';
$language["SET_TTS_TRAVEL"] = 'TravelTime2TTS';
$language['SET_TTS_PAGE_TITLE'] = 'MS4L TTS / Klingel Einstellungen';
$language['SET_TTS_TITLE'] = 'Text2Speech / Klingel Einstellungen';
$language["SET_TTS_TITLE_CACHE"] = 'TTS Cache';
$language["SET_TTS_TITLE_RING"] = 'Klingel / Alarm / Wecker Standards';
$language['SET_TTS_TITLE_DEFAULT'] = 'TTS Standards';
$language['SET_TTS_DEFAULT_PROVIDER'] = 'Provider';
$language['SET_TTS_DEFAULT_VOLUME'] = 'Lautstärke';
$language["SET_TTS_DEFAULT_AUTOPLAY"] = 'AutoPlay nach Wiedergabe';
$language["SET_TTS_DEFAULT_AUTOSYNC"] = 'AutoSync nach Wiedergabe';
$language['SET_TTS_DEFAULT_OVERLAY'] = 'Overlay aktivieren';
$language['SET_TTS_DEFAULT_OVERLAYDROP'] = 'Overlay-Drop';
$language['SET_TTS_DEFAULT_VOLUME'] = 'Lautstärke';
$language['SET_TTS_DEFAULT_TIMEOUT'] = 'Timeout';
$language["SET_TTS_DEFAULT_RING_FILE"] = 'Klingelton';
$language["SET_TTS_DEFAULT_DELETE_FILEAFTER"] = 'Unbenutzte TTS-Dateien löschen (Tage)';
$language["SET_TTS_DEFAULT_TTSCACHE"] = 'TTS Cache';
$language["SET_TTS_DEFAULT_TTSCACHEFILE"] = 'Datei(en)';
$language["SET_TTS_DEFAULT_CLEARCACHE"] = 'TTS - Cache löschen';
$language['SET_TTS_TITLE_POLLY'] = 'Polly';
$language['SET_TTS_TITLE_VOICERSS'] = 'VoiceRSS';
$language['SET_TTS_TITLE_RESPONSIVEVOICE'] = 'Responsive Voice';
$language["SET_TTS_POLLY_ACCESSID"] = 'Access Key ID';
$language["SET_TTS_POLLY_SECRETKEY"] = 'Secret Access Key';
$language["SET_TTS_POLLY_LANG"] = 'Sprache / Stimme';
$language["SET_TTS_VOICERSS_TOKEN"] = 'Token';
$language["SET_TTS_VOICERSS_QUALITY"] = 'Audio-Qualität';
$language["SET_TTS_VOICERSS_LANG"] = 'Sprache / Stimme';
$language["SET_TTS_REQ_DEFAULT_VOLUME"] = 'Volume muss zwischen 1-100 liegen.';
$language["SET_TTS_REQ_DEFAULT_TIMEOUT"] = 'Timeout muss zwischen 10-360 liegen.';;
$language["SET_TTS_REQ_DEFAULT_DELETE_FILEAFTER"] = 'Tage müssen zwischen 0-365 liegen.';

//Weather2TTS
$language['SET_W2T_PAGE_TITLE'] = 'MS4L - Weather2TTS Einstellungen';
$language["SET_W2T_TITLE"] = 'Weather2TTS Einstellungen';
$language["SET_W2T_TITLE_BASIC"] = 'Basis Einstellungen';
$language["SET_W2T_TITLE_DEFAULT"] = 'Standard Einstellungen';
$language["SET_W2T_TITLE_TEXT"] = 'Texte';
$language["SET_W2T_DEFAULT_APIKEY"] = 'API-Key - Weatherbit.io';
$language["SET_W2T_DEFAULT_LAT"] = 'Breitengrad (Latitude)';
$language["SET_W2T_DEFAULT_LON"] = 'Längengrad (Longitude)';
$language["SET_W2T_DEFAULT_FINDGEO"] = 'Geodaten finden';
$language["SET_W2T_DEFAULT_STANDARD_OUT"] = 'Ausgabe'; 
$language["SET_W2T_DEFAULT_RAIN_THRESHOLD"] = 'Windschwelle';
$language["SET_W2T_DEFAULT_WIND_THRESHOLD"] = 'Regenschwelle';
$language["SET_W2T_DEFAULT_TEXT"] = 'Texte bearbeiten';
$language["SET_W2T_DEFAULT_TEXTCHOOSE"] = 'Text wählen';
$language["SET_W2T_DEFAULT_OVERWRITE"] = 'Texte mit Sprach-Standard überschrieben';
$language["SET_W2T_TEXT_RAIN"] = 'Regen (today)';
$language["SET_W2T_TEXT_WIND"] = 'Wind (today)';
$language["SET_W2T_TEXT_RAINFC"] = 'Regen (forecast)';
$language["SET_W2T_TEXT_WINDFC"] = 'Wind (forecast)';
$language["SET_W2T_TEXT_NOW"] = 'aktuell (now)';
$language["SET_W2T_TEXT_TODAY"] = 'heute (today)';
$language["SET_W2T_TEXT_TODAY22"] = 'heute nach 22 Uhr (today)';
$language["SET_W2T_TEXT_TODAYTOMORROW"] = 'heute und morgen (today_forecast)';
$language["SET_W2T_TEXT_FORECAST"] = 'Morgen (forecast)';
$language["SET_W2T_TEXT_DAYTIME6_11"] = 'Tageszeit 6-11 Uhr (daytime)';
$language["SET_W2T_TEXT_DAYTIME11_17"] = 'Tageszeit 11-17 Uhr (daytime)';
$language["SET_W2T_TEXT_DAYTIME17_22"] = 'Tageszeit 17-22 Uhr (daytime)';
$language["SET_W2T_TEXT_DAYTIME22_6"] = 'Tageszeit 22-6 Uhr (daytime)';
$language["SET_W2T_TEXT_CUSTOM1"] = 'Benutzerdefiniert 1 (custom_1)';
$language["SET_W2T_TEXT_CUSTOM2"] = 'Benutzerdefiniert 2 (custom_2)';
$language["SET_W2T_TEXT_ASTRONOMIC"] = 'Astro (astronomic)';
$language["SET_W2T_TEXT_SUNSET"] = 'Sonnen Auf- und Untergang (sunset)';
$language["SET_W2T_TEXT_MOONPHASE"] = 'Mondphase (moonphase)';
$language["SET_W2T_REQ_DEFAULT_APIKEY"] = 'API muss vergeben werden';
$language["SET_W2T_REQ_DEFAULT_LAT"] = 'Breitengrad muss zwischen -90 und 180°';
$language["SET_W2T_REQ_DEFAULT_LON"] = 'Längengrad muss zwischen -90 und 180°';
$language["SET_W2T_REQ_DEFAULT_RAIN_THRESHOLD"] = 'Regenschwelle muss zwischen 0-100% liegen.';
$language["SET_W2T_REQ_DEFAULT_WIND_THRESHOLD"] = 'Windschwelle muss zwischen 0-100% liegen.';
//Weather2TTS Standard-Texts
//These Texts can be load as standards in WebUi, and changes can be made in the WebUI. Changes will be saved in the Config-File 
//All words between {} are variables and may not be modified!!!
$language["WTTS_TEXT_RAIN_TODAY"] = 'Die Regenwahrscheinlichkeit liegt bei {chance_of_rain_today} Prozent.';
$language["WTTS_TEXT_WIND_TODAY"] = 'Es weht {wind_text_today} aus Richtung {wind_direction_today} mit Geschwindigkeiten von bis zu {wind_speed_today} km/h.';
$language["WTTS_TEXT_RAIN_TOMORROW"] = 'Die Regenwahrscheinlichkeit liegt bei {chance_of_rain_tomorrow} Prozent.';
$language["WTTS_TEXT_WIND_TOMORROW"] = 'Es weht {wind_text_tomorrow} aus Richtung {wind_direction_tomorrow} mit Geschwindigkeiten von bis zu {wind_speed_tomorrow} km/h.';
$language["WTTS_TEXT_NOW"] = 'Jetzt {condition_now} bei {temp_now} Grad. Die Luftfeuchtigkeit liegt bei {relative_humidity_now}  Prozent, der Luftdruck bei {pressure_now} Millibar. Der Wind weht aus Richtung {wind_direction_now} mit {wind_speed_now} Km/h mit vereinzelten Böen von {wind_gust_now} Km/h. Die Regenmenge ist {rain_fall_now} mm je Quadratmeter. Die UV-Strahlung ist {uv_radiation_now}.';
$language["WTTS_TEXT_TODAY"] = 'Heute {condition_today}, die Höchsttemperatur beträgt {temp_high_today} Grad, die Tiefsttemperatur {temp_low_today} Grad. {rain_text_today}{wind_text_today}';
$language["WTTS_TEXT_TODAY2"] = 'Jetzt {condition_now}, die aktuelle Temperatur beträgt {temp_now} Grad. Morgen {condition_tomorrow}, die Temperatur liegt zwischen {temp_low_tomorrow} und {temp_high_tomorrow} Grad. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_TODAY_FORECAST"] = 'Heute {condition_today}, die Höchsttemperatur beträgt {temp_high_today} Grad, die Tiefsttemperatur {temp_low_today} Grad. {rain_text_today} {wind_text_today}. Morgen {condition_tomorrow}, die Temperatur liegt zwischen {temp_low_tomorrow} und {temp_high_tomorrow} Grad. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_FORECAST"] = 'Morgen {condition_tomorrow}, die Temperatur liegt zwischen {temp_low_tomorrow} und {temp_high_tomorrow} Grad. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_DAYTIME_6_11"] = 'Heute Vormittag {condition_now}, die Höchsttemperatur beträgt voraussichtlich {temp_high_today} Grad, die aktuelle Temperatur {temp_now} Grad. {rain_text_today} {wind_text_today}';
$language["WTTS_TEXT_DAYTIME_11_17"] = 'Heute Nachmittag {condition_now}. Die momentane Außentemperatur beträgt {temp_now} Grad. {rain_text_today} {wind_text_today}';
$language["WTTS_TEXT_DAYTIME_17_22"] = 'In den Abendstunden {condition_now}. Die aktuelle Außentemperatur ist {temp_now} Grad, die zu erwartende Tiefsttemperatur heute Abend beträgt {temp_low_today} Grad. {rain_text_today} {wind_text_today}';
$language["WTTS_TEXT_DAYTIME_22"] = 'Jetzt hat es {temp_now} Grad. Morgen {condition_tomorrow}, die Temperatur liegt zwischen {temp_low_tomorrow} und {temp_high_tomorrow} Grad. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_CUSTOM_1"] = 'Dein Text 1';
$language["WTTS_TEXT_CUSTOM_2"] = 'Dein Text 2';
$language["WTTS_TEXT_ASTRO"] = 'Sonnenaufgang ist heute um {sunriseh} Uhr {sunrisem}, Sonnenuntergang um {sunseth} Uhr {sunsetm}, die aktuelle Mondphase ist {moonphase}, der Beleuchtungsgrad des Monds ist bei {moonillu}%..';
$language["WTTS_TEXT_SUNSET"] = 'Sonnenaufgang ist heute um {sunriseh} Uhr {sunrisem}, Sonnenuntergang um {sunseth} Uhr {sunsetm}.';
$language["WTTS_TEXT_MOONPHASE"] = 'Die aktuelle Mondphase ist {moonphase} , der Beleuchtungsgrad ist bei {moonillu}%.';

$language["WTTS_CONDITION_200"]	= 'haben wir Gewitter mit leichtem Regen';
$language["WTTS_CONDITION_201"]	= 'haben wir Gewitter mit Regen';
$language["WTTS_CONDITION_202"]	= 'haben wir Gewitter mir starkem Regen';
$language["WTTS_CONDITION_230"]	= 'haben wir Gewitter mit leichten Regenschauern';
$language["WTTS_CONDITION_231"]	= 'haben wir Gewitter mit Regenschauern';
$language["WTTS_CONDITION_232"]	= 'haben wir Gewitter mit starken Regenschauern';
$language["WTTS_CONDITION_233"]	= 'haben wir Gewitter mit Hagel';
$language["WTTS_CONDITION_300"]	= 'haben wir leichten Nieselregen';
$language["WTTS_CONDITION_301"]	= 'haben wir Nieselregen';
$language["WTTS_CONDITION_302"]	= 'haben wir starken Nieselregen';
$language["WTTS_CONDITION_500"]	= 'ist es leicht regnerisch';
$language["WTTS_CONDITION_501"]	= 'ist es mäßig regnerisch';
$language["WTTS_CONDITION_502"]	= 'ist es stark regnerisch';
$language["WTTS_CONDITION_511"]	= 'haben wir gefrierenden Regen';
$language["WTTS_CONDITION_520"]	= 'haben wir leichte Regengüsse';
$language["WTTS_CONDITION_521"]	= 'haben wir Regengüsse';
$language["WTTS_CONDITION_522"]	= 'haben wir starke Regengüsse';
$language["WTTS_CONDITION_600"]	= 'schneit es leicht';
$language["WTTS_CONDITION_601"]	= 'schneit es';
$language["WTTS_CONDITION_602"]	= 'schneit es stark';
$language["WTTS_CONDITION_610"]	= 'haben wir einen Mix aus Schnee und Regen';
$language["WTTS_CONDITION_611"]	= 'haben wir Schneeregen';
$language["WTTS_CONDITION_612"]	= 'haben wir starken Graupelschauer';
$language["WTTS_CONDITION_621"]	= 'haben wir Schneeschauer';
$language["WTTS_CONDITION_622"]	= 'haben wir starken Schneeschauer';
$language["WTTS_CONDITION_623"]	= 'haben wir böig';
$language["WTTS_CONDITION_700"]	= 'ist es trübe';
$language["WTTS_CONDITION_711"]	= 'ist es trübe';
$language["WTTS_CONDITION_721"]	= 'ist es trübe';
$language["WTTS_CONDITION_731"]	= 'ist es trübe';
$language["WTTS_CONDITION_741"]	= 'ist es neblig';
$language["WTTS_CONDITION_751"]	= 'haben wir überfrierender Nebel';
$language["WTTS_CONDITION_800"]	= 'ist es leicht bewölkt';
$language["WTTS_CONDITION_801"]	= 'haben wir verstreute Bewölkung';
$language["WTTS_CONDITION_802"]	= 'haben wir eine aufgelockerte Bewölkung';
$language["WTTS_CONDITION_803"]	= 'ist es bedeckt mit einzelnen Lücken';
$language["WTTS_CONDITION_804"]	= 'ist es bedeckt';
$language["WTTS_CONDITION_900"]	= 'ist das Wetter noch unbekannt';

$language["WTTS_DIRECTION_UKNOWN"] = 'unbestimmt';
$language["WTTS_DIRECTION_E"] = 'Ost';
$language["WTTS_DIRECTION_ENE"] = 'OstNordOst';
$language["WTTS_DIRECTION_ESE"] = 'OstSüdOst';
$language["WTTS_DIRECTION_N"] = 'Nord';
$language["WTTS_DIRECTION_NE"] = 'NordOst';
$language["WTTS_DIRECTION_NNE"] = 'NordNordOst';
$language["WTTS_DIRECTION_NNW"] = 'NordNordWest';
$language["WTTS_DIRECTION_NW"] = 'NordWest';
$language["WTTS_DIRECTION_S"] = 'Süd';
$language["WTTS_DIRECTION_SE"] = 'SüdOst';
$language["WTTS_DIRECTION_SSE"] = 'SüdSüdOst';
$language["WTTS_DIRECTION_SSW"] = 'SüdSüdWest';
$language["WTTS_DIRECTION_SW"] = 'SüdWest';
$language["WTTS_DIRECTION_W"] = 'West';
$language["WTTS_DIRECTION_WNW"] = 'WestNordWest';
$language["WTTS_DIRECTION_WSW"] = 'WestSüdWest';
//Beaufort Skala
$language["WTTS_WIND_1"] = 'ein leiser Zug';
$language["WTTS_WIND_2"] = 'eine leichte Brise';
$language["WTTS_WIND_3"] = 'eine schwache Brise';
$language["WTTS_WIND_4"] = 'ein mäßiger Wind';
$language["WTTS_WIND_5"] = 'ein frischer Wind';
$language["WTTS_WIND_6"] = 'ein starker Wind';
$language["WTTS_WIND_7"] = 'ein steifer Wind';
$language["WTTS_WIND_8"] = 'ein stürmischer Wind';
$language["WTTS_WIND_9"] = 'ein Sturm';
$language["WTTS_WIND_10"] = 'ein schwerer Sturm';
$language["WTTS_WIND_11"] = 'ein orkanartiger Sturm';
$language["WTTS_WIND_11"] = 'ein Orkan';
      
$language["WTTS_MOON_1"] = 'NeuMond';
$language["WTTS_MOON_2"] = 'zunehmender Sichelmond';
$language["WTTS_MOON_3"] = 'zunehmender Halbmond (Erstes Viertel)';
$language["WTTS_MOON_4"] = 'zunehmender Dreiviertelmond';
$language["WTTS_MOON_5"] = 'Vollmond';
$language["WTTS_MOON_6"] = 'abnehmender Dreiviertelmond';
$language["WTTS_MOON_7"] = 'abnehmender Halbmond (Letztes Viertel)';
$language["WTTS_MOON_8"] = 'abnehmender Sichelmond';

//CTS
$language["CTS_HOUR_1"] = 'ein'; //ein Uhr
$language["CTS_MINUTE_1"] = 'eine Minute';
$language["CTS_MINUTES"] = 'Minuten';
$language["CTS_TEXT"] = 'Es ist jetzt {hours} Uhr und {minutes}.';

//Traveltiime2TTS
$language['TT2T_PAGE_TITLE'] = 'MS4L - Traveltime2TTS Einstellungen';
$language["TT2T_TITLE"] = 'Traveltime2TTS Einstellungen';
$language["TT2T_TITLE_BASIC"] = 'Basis Einstellungen';
$language["TT2T_TITLE_DEFAULT"] = 'Standard Einstellungen';
$language["TT2T_TITLE_TEXT"] = 'Text';
$language["TT2T_DEFAULT_APIKEY"] = 'API-Key - Google';
$language["TT2T_DEFAULT_TRAFFIC"] = 'Verkehrsfluss berücksichtigen'; 
$language["TT2T_DEFAULT_TRAFFICCALC"] = 'Verkehrsfluss-Berechnung';
$language["TT2T_DEFAULT_OVERWRITE"] = 'Texte mit Sprach-Standard überschrieben';
$language["TT2T_TEXT"] = 'Reisezeit Text bearbeiten';
$language["TT2T_REQ_DEFAULT_APIKEY"] = 'API muss vergeben werden';
$language["TT2T_TEXT"] = 'Für die Stecke von {start} nach {arrival} mit {distance} km, benötigst du aktuell {duration}.';

//EventCreator
$language["EC_EVNETS"] = 'Events';
$language["EC_TTS"] = 'Text2Speech';
$language["EC_CTS"] = 'Clock2Speech';
$language["EC_RING"] = 'Klingel/Wecker/Alarm';
$language["EC_WEATHER"] = 'Weather2Speech';
$language["EC_TRAVEL"] = 'Traveltime2Speech';
$language['EC_PAGE_TITLE'] = 'MS4L - EventCreator';
$language["EC_TITLE"] = 'EventCreator';
$language["EC_TITLE_VQ_ADDRESS"] = 'Virtueller Ausgang Adresse';
$language["EC_TITLE_VCQ_COMMANDON"] = 'Virtueller Ausgangsbefehl "Befehl bei EIN"';
$language["EC_TITLE_TTS"] = 'Text2Speech';
$language["EC_TITLE_TTS_TEXT"] = 'TTS-Text';
$language["EC_TITLE_CTS_TEXT"] = 'Text nach Zeitansage';
$language["EC_TITLE_W2T_TODO"] = 'Wetteransage ToDo';
$language["EC_TITLE_TT2T"] = 'Reiseangaben';
$language["EC_TT2T_FROM"] = 'Von Adresse';
$language["EC_TT2T_TO"] = 'Nach Adresse';
$language["EC_TT2T_TRAFFIC"] = 'Verkehrsfluss berücksichtigen';
$language["EC_TT2T_TRAFFICCALC"] = 'Verkehrsfluss-Berechnung';
$language["EC_TT2T_DEPARTURE_TIME"] = 'Abfahrtzeit (24h)';
$language["EC_TITLE_RING_FILE"] = 'Datei';
$language["EC_TITLE_ZONEINTERN"] = 'Zonen intern';
$language["EC_TITLE_ZONEEXTERN"] = 'Zonen extern';
$language["EC_TITLE_OPTIONSBASIC"] = 'Basics';
$language["EC_TITLE_OPTIONSAFTER"] = 'Nach dem Event';
$language["EC_TITLE_OPTIONOVERLAY"] = 'Overlay';
$language["EC_TITLE_OPTIONSPECIAL"] = 'Spezial';
$language["EC_ALL_ZONEINTERN"] = 'Alle internen Zonen';
$language["EC_ALL_ZONEEXTERN"] = 'Alle externen Zonen';
$language["EC_VOLUME"] = 'Lautstärke';
$language["EC_REPEAT"] = 'Repeat Event';
$language["EC_SIGNAL"] = 'Signal';
$language["EC_TIMEOUT"] = 'Timeout';
$language["EC_REIMP"] = 'Ready Impulse';
$language["EC_AUTOPLAY"] = 'Autoplay';
$language["EC_SYNC"] = 'Sync';
$language["EC_REPEAT_RESET"] = 'Repeat Reset';
$language["EC_OVERLAY"] = 'Overlay';
$language["EC_OVERLAYDROP"] = 'Overlay Drop';
$language["EC_PRIO"] = 'Priorität';
$language["EC_QUEUE"] = 'Warteschlangen';
$language["EC_SYSTEM_STANDARD"] = 'System Standard wird genutzt';
$language["EC_NOSIGNAL"] = 'Kein Signal abspielen';
$language["EC_GENERATE"] = 'Event generieren';
$language["EC_REQ_TEXT"] = 'Es muss ein Text eingeben werden.';
$language["EC_REQ_VOLUME"] = 'Wert muss zwischen 1-100 liegen.';
$language["EC_REQ_TIMEOUT"] = 'Wert muss größer 10 sein.';
$language["EC_REQ_REIMP"] = 'Wert muss größer 1 sein.';
$language["EC_REQ_ADDRESS"] = 'Adresse muss eingegeben werden';
$language["EC_REQ_DEPARTURE_TIME"] = 'Zeitformat muss h:m (Stunden:Minuten)';
$language["EC_PH_DEPARTURE_TIME"] = 'h:m (leer = aktuelle Uhrzeit (now))';
$language["EC_STOP"] = 'Stop';

//Zone-Tester
?>
